use-auth-secret
static-auth-secret={{ key "secrets/chat/coturn/static-auth" | trimSpace }}
realm=turn.deuxfleurs.fr

# VoIP traffic is all UDP. There is no reason to let users connect to arbitrary TCP endpoints via the relay.
#no-tcp-relay

# don't let the relay ever try to connect to private IP address ranges within your network (if any)
# given the turn server is likely behind your firewall, remember to include any privileged public IPs too.
#denied-peer-ip=10.0.0.0-10.255.255.255
#denied-peer-ip=192.168.0.0-192.168.255.255
#denied-peer-ip=172.16.0.0-172.31.255.255

# consider whether you want to limit the quota of relayed streams per user (or total) to avoid risk of DoS.
user-quota=12 # 4 streams per video call, so 12 streams = 3 simultaneous relayed calls per user.
total-quota=1200

min-port=49152
max-port=49252
