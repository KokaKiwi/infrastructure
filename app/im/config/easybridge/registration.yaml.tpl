id: Easybridge
url: http://easybridge-api.service.2.cluster.deuxfleurs.fr:8321
as_token: {{ key "secrets/chat/easybridge/as_token" | trimSpace }}
hs_token: {{ key "secrets/chat/easybridge/hs_token" | trimSpace }}
sender_localpart: _ezbr_
rate_limited: false
namespaces:
  users:
  - exclusive: true
    regex: '@.*_ezbr_'
  aliases:
  - exclusive: true
    regex: '#.*_ezbr_'
  rooms: []
