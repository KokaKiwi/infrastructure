job "im" {
  datacenters = ["dc1"]
  type = "service"
  priority = 60

  group "matrix" {
    count = 1

    network {
      port "client_port" { static = 8008 }
      port "federation_port" { static = 8448 }
    }

    task "synapse" {
      driver = "docker"

      config {
        image = "superboum/amd64_synapse:v47"
        network_mode = "host"
        readonly_rootfs = true
        ports = [ "client_port", "federation_port" ]
        command = "python"
        args = [
          "-m", "synapse.app.homeserver",
          "-n",
          "-c", "/etc/matrix-synapse/homeserver.yaml"
        ]
        volumes = [
          "secrets/conf:/etc/matrix-synapse",
          "/tmp/synapse-media:/var/lib/matrix-synapse/media",
          "/tmp/synapse-uploads:/var/lib/matrix-synapse/uploads",
          "/tmp/synapse-logs:/var/log/matrix-synapse",
          "/tmp/synapse:/tmp"
        ]
      }

      template {
        data = file("../config/synapse/homeserver.yaml")
        destination = "secrets/conf/homeserver.yaml"
      }

      template {
        data = file("../config/easybridge/registration.yaml.tpl")
        destination = "secrets/conf/easybridge_registration.yaml"
      }

      template {
        data = file("../config/synapse/log.yaml")
        destination = "secrets/conf/log.yaml"
      }

      template {
        data = file("../config/synapse/conf.d/server_name.yaml")
        destination = "secrets/conf/server_name.yaml"
      }

      template {
        data = file("../config/synapse/conf.d/report_stats.yaml")
        destination = "secrets/conf/report_stats.yaml"
      }

      # --- secrets ---
      template {
        data = "{{ key \"secrets/chat/synapse/homeserver.tls.crt\" }}"
        destination = "secrets/conf/homeserver.tls.crt"
      }

      template {
        data = "{{ key \"secrets/chat/synapse/homeserver.tls.dh\" }}"
        destination = "secrets/conf/homeserver.tls.dh"
      }

      template {
        data = "{{ key \"secrets/chat/synapse/homeserver.tls.key\" }}"
        destination = "secrets/conf/homeserver.tls.key"
      }

      template {
        data = "{{ key \"secrets/chat/synapse/homeserver.signing.key\" }}"
        destination = "secrets/conf/homeserver.signing.key"
      }

      env {
        SYNAPSE_CACHE_FACTOR = 1
      }

      resources {
        cpu = 1000
        memory = 2000
      }

      service {
        name = "synapse-client"
        port = "client_port"
        address_mode = "host"
        tags = [
          "matrix",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https",
          "traefik.frontend.rule=Host:im.deuxfleurs.fr;PathPrefix:/_matrix,/_synapse",
          "traefik.frontend.headers.customResponseHeaders=Access-Control-Allow-Origin: *",
          "traefik.frontend.priority=100"
        ]
        check {
          type = "tcp"
          port = "client_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        name = "synapse-federation"
        port = "federation_port"
        address_mode = "host"
        tags = [
          "matrix",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https",
          "traefik.frontend.rule=Host:deuxfleurs.fr;PathPrefix:/_matrix",
          "traefik.frontend.priority=100"
        ]
      }
    }


    task "media-async-upload" {
      driver = "docker"

      config {
        image = "superboum/amd64_synapse:v47"
        readonly_rootfs = true
        command = "/usr/local/bin/matrix-s3-async"
        work_dir = "/tmp"
        volumes = [
          "/tmp/synapse-media:/var/lib/matrix-synapse/media",
          "/tmp/synapse-uploads:/var/lib/matrix-synapse/uploads",
          "/tmp/synapse:/tmp"
        ]
      }

      resources {
        cpu = 100
        memory = 200
      }

      template {
        data = <<EOH
AWS_ACCESS_KEY_ID={{ key "secrets/chat/synapse/s3_access_key" | trimSpace }}
AWS_SECRET_ACCESS_KEY={{ key "secrets/chat/synapse/s3_secret_key" | trimSpace }}
AWS_DEFAULT_REGION=garage
PG_USER={{ key "secrets/chat/synapse/postgres_user" | trimSpace }}
PG_PASS={{ key "secrets/chat/synapse/postgres_pwd" | trimSpace }}
PG_DB={{ key "secrets/chat/synapse/postgres_db" | trimSpace }}
PG_HOST=psql-proxy.service.2.cluster.deuxfleurs.fr
PG_PORT=5432
EOH
        destination = "secrets/env"
        env = true
      }
    }
  }


  group "riotweb" {
    count = 1

    network {
      port "web_port" { to = 8043 }
    }

    task "server" {
      driver = "docker"
      config {
        image = "superboum/amd64_riotweb:v24"
        ports = [ "web_port" ]
        volumes = [
          "secrets/config.json:/srv/http/config.json"
        ]
      }

      template {
        data = file("../config/riot_web/config.json")
        destination   = "secrets/config.json"
      }

      resources {
        memory = 21
      }

      service {
        tags = [
          "webstatic",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https",
          "traefik.frontend.rule=Host:im.deuxfleurs.fr,riot.deuxfleurs.fr;PathPrefix:/",
          "traefik.frontend.priority=10"
        ]
        port = "web_port"
        address_mode = "host"
        name = "webstatic"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

