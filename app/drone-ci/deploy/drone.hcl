job "drone-ci" {
  datacenters = ["dc1"]
  type = "service"

  group "server" {
    count = 1

    network {
      port "web_port" {
		  to = 80
	  }
    }

    task "drone_server" {
      driver = "docker"
      config {
        image = "drone/drone:2.0.4"
        ports = [ "web_port" ]
      }

	  template {
		  data = <<EOH
DRONE_GITEA_SERVER=https://git.deuxfleurs.fr
DRONE_GITEA_CLIENT_ID={{ key "secrets/drone-ci/oauth_client_id" }}
DRONE_GITEA_CLIENT_SECRET={{ key "secrets/drone-ci/oauth_client_secret" }}
DRONE_RPC_SECRET={{ key "secrets/drone-ci/rpc_secret" }}
DRONE_SERVER_HOST=drone.deuxfleurs.fr
DRONE_SERVER_PROTO=https
DRONE_DATABASE_SECRET={{ key "secrets/drone-ci/db_enc_secret" }}
DRONE_COOKIE_SECRET={{ key "secrets/drone-ci/cookie_secret" }}
AWS_ACCESS_KEY_ID={{ key "secrets/drone-ci/s3_ak" }}
AWS_SECRET_ACCESS_KEY={{ key "secrets/drone-ci/s3_sk" }}
AWS_DEFAULT_REGION=garage
AWS_REGION=garage
DRONE_S3_BUCKET={{ key "secrets/drone-ci/s3_bucket" }}
DRONE_S3_ENDPOINT=https://garage.deuxfleurs.fr
DRONE_S3_PATH_STYLE=true
DRONE_DATABASE_DRIVER=postgres
DRONE_DATABASE_DATASOURCE=postgres://{{ key "secrets/drone-ci/db_user" }}:{{ key "secrets/drone-ci/db_pass" }}@psql-proxy.service.2.cluster.deuxfleurs.fr:5432/drone?sslmode=disable
DRONE_USER_CREATE=username:lx-admin,admin:true
DRONE_LOGS_TEXT=true
DRONE_LOGS_PRETTY=true
DRONE_LOGS_DEBUG=true
DOCKER_API_VERSION=1.39
EOH
		destination = "secrets/env"
		env = true
	  }

      resources {
        cpu = 100
        memory = 100
      }

      service {
        name = "drone"
        tags = [
          "drone",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:drone.deuxfleurs.fr",
        ]
        port = "web_port"
        address_mode = "host"
        check {
          type = "http"
          protocol = "http"
          port = "web_port"
          path = "/"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "600s"
            ignore_warnings = false
          }
        }
      }
    }
  }

/*
  group "runner" {
    count = 3

	constraint {
	  operator  = "distinct_hosts"
	  value     = "true"
	}

    task "drone_runner" {
      driver = "docker"
      config {
		network_mode = "host"

        #image = "drone/drone-runner-nomad:latest"
        image = "drone/drone-runner-docker:1.6.3"

        volumes = [
          "/var/run/docker.sock:/var/run/docker.sock"
        ]
      }

	  template {
		  data = <<EOH
DRONE_RPC_SECRET={{ key "secrets/drone-ci/rpc_secret" }}
DRONE_RPC_HOST=drone.deuxfleurs.fr
DRONE_RPC_PROTO=https
DRONE_RUNNER_NAME={{ env "node.unique.name" }}
DRONE_DEBUG=true
NOMAD_ADDR=http://nomad-client.service.2.cluster.deuxfleurs.fr:4646
DOCKER_API_VERSION=1.39
EOH
		destination = "secrets/env"
		env = true
	  }

      resources {
        memory = 40
        cpu = 50
      }
	}
  }
  */
}
