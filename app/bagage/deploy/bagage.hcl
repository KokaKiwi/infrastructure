job "bagage" {
  datacenters = ["dc1"]
  type = "service"
  priority = 90

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "main" {
    count = 1

    network {
      port "web_port" { to = 8080 }
    }

    task "server" {
      driver = "docker"
      config {
        image = "superboum/amd64_bagage:v8"
        readonly_rootfs = true
        ports = [ "web_port" ]
      }

      env {
        BAGAGE_LDAP_ENDPOINT = "bottin2.service.2.cluster.deuxfleurs.fr:389"
      }

      resources {
        memory = 500
      }

      service {
        name = "bagage"
        tags = [
          "bagage",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:bagage.deuxfleurs.fr",
        ]
        port = "web_port"
        address_mode = "host"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

