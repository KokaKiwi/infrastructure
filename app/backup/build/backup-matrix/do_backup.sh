#!/bin/sh

set -x -e

cd /root

chmod 0600 .ssh/id_ed25519

cat > .ssh/config <<EOF
Host backuphost
	HostName $TARGET_SSH_HOST
	Port $TARGET_SSH_PORT
	User $TARGET_SSH_USER
EOF

echo "export sql"
export PGPASSWORD=$REPL_PSQL_PWD
pg_basebackup \
		--pgdata=- \
		--format=tar \
		--max-rate=1M \
		--no-slot \
		--wal-method=none \
		--gzip \
		--compress=8 \
		--checkpoint=spread \
		--progress \
		--verbose \
		--status-interval=10 \
		--username=$REPL_PSQL_USER \
		--port=5432 \
		--host=psql-proxy.service.2.cluster.deuxfleurs.fr | \
	age -r "$(cat /root/.ssh/id_ed25519.pub)" | \
	ssh backuphost "cat > $TARGET_SSH_DIR/matrix/db-$(date --iso-8601=minute).gz.age"

MATRIX_MEDIA="/mnt/glusterfs/chat/matrix/synapse/media"
echo "export local_content"
tar -vzcf - ${MATRIX_MEDIA} | \
	age -r "$(cat /root/.ssh/id_ed25519.pub)" | \
	ssh backuphost "cat > $TARGET_SSH_DIR/matrix/media-$(date --iso-8601=minute).gz.age"
