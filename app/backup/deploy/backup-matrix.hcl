job "backup_manual_matrix" {
	datacenters = ["dc1"]

	type = "batch"

	task "backup-matrix" {
		driver = "docker"

			config {
				image = "superboum/backup_matrix:4"
				volumes = [
					"secrets/id_ed25519:/root/.ssh/id_ed25519",
					"secrets/id_ed25519.pub:/root/.ssh/id_ed25519.pub",
					"secrets/known_hosts:/root/.ssh/known_hosts",
					"/mnt/glusterfs/chat/matrix/synapse/media:/mnt/glusterfs/chat/matrix/synapse/media"
				]
				network_mode = "host"
			}

		env {
			CONSUL_HTTP_ADDR = "http://consul.service.2.cluster.deuxfleurs.fr:8500"
		}

		template {
			data = <<EOH
TARGET_SSH_USER={{ key "secrets/backup/target_ssh_user" }}
TARGET_SSH_PORT={{ key "secrets/backup/target_ssh_port" }}
TARGET_SSH_HOST={{ key "secrets/backup/target_ssh_host" }}
TARGET_SSH_DIR={{ key "secrets/backup/target_ssh_dir" }}
REPL_PSQL_USER={{ key "secrets/postgres/keeper/pg_repl_username" }}
REPL_PSQL_PWD={{ key "secrets/postgres/keeper/pg_repl_pwd" }}
EOH

			destination = "secrets/env_vars"
			env = true
		}

		template {
			data = "{{ key \"secrets/backup/id_ed25519\" }}"
			destination = "secrets/id_ed25519"
		}
		template {
			data = "{{ key \"secrets/backup/id_ed25519.pub\" }}"
			destination = "secrets/id_ed25519.pub"
		}
		template {
			data = "{{ key \"secrets/backup/target_ssh_fingerprint\" }}"
			destination = "secrets/known_hosts"
		}

		resources {
			memory = 200
		}

		restart {
			attempts = 2
			interval = "30m"
			delay = "15s"
			mode = "fail"
		}
	}
}
