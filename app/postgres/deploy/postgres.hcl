job "postgres13.3" {
  datacenters = ["dc1"]
  type = "system"
  priority = 90

  update {
    max_parallel = 1
    min_healthy_time  = "2m"
    healthy_deadline  = "5m"
    auto_revert = true
  }

  group "postgres" {
    network {
      port "psql_proxy_port" { static = 5432 }
      port "psql_port" { static = 5433 }
    }

    task "sentinel" {
      driver = "docker"

      config {
        image = "superboum/amd64_postgres:v10"
        network_mode = "host" 
        readonly_rootfs = false
        command = "/usr/local/bin/stolon-sentinel"
        args = [
          "--cluster-name", "chelidoine",
          "--store-backend", "consul",
          "--store-endpoints", "http://consul.service.2.cluster.deuxfleurs.fr:8500",
        ]
      }
      resources {
        memory = 100
      }
    }

    task "proxy" {
      driver = "docker"

      config {
        image = "superboum/amd64_postgres:v10"
        network_mode = "host" 
        readonly_rootfs = false
        command = "/usr/local/bin/stolon-proxy"
        args = [
          "--cluster-name", "chelidoine",
          "--store-backend", "consul",
          "--store-endpoints", "http://consul.service.2.cluster.deuxfleurs.fr:8500",
          "--port", "${NOMAD_PORT_psql_proxy_port}",
          "--listen-address", "0.0.0.0",
          "--log-level", "info"
        ]
        ports = [ "psql_proxy_port" ]
      }

      resources {
        memory = 100
      }

      service {
        tags = ["sql"]
        port = "psql_proxy_port"
        address_mode = "host"
        name = "psql-proxy"
        check {
          type = "tcp"
          port = "psql_proxy_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "10m"
            ignore_warnings = false
          }
        }
      }
    }

    task "keeper" {
      driver = "docker"

      config {
        image = "superboum/amd64_postgres:v10"
        network_mode = "host" 
        readonly_rootfs = false
        command = "/usr/local/bin/stolon-keeper"
        args = [
          "--cluster-name", "chelidoine",
          "--store-backend", "consul",
          "--store-endpoints", "http://consul.service.2.cluster.deuxfleurs.fr:8500",
          "--data-dir", "/mnt/persist",
          "--pg-su-password", "${PG_SU_PWD}",
          "--pg-repl-username", "${PG_REPL_USER}",
          "--pg-repl-password", "${PG_REPL_PWD}",
          "--pg-listen-address", "${attr.unique.network.ip-address}",
          "--pg-port", "${NOMAD_PORT_psql_port}",
          "--pg-bin-path", "/usr/lib/postgresql/13/bin/"
        ]
        ports = [ "psql_port" ]
        volumes = [
          "/mnt/ssd/postgres:/mnt/persist",
          "/mnt/storage/postgres_extended:/mnt/slow"
        ]
      }

      template {
        data = file("../config/keeper/env.tpl")
        destination = "secrets/env"
        env = true
      }

      resources {
        memory = 1000
      }

      service {
        tags = ["sql"]
        port = "psql_port"
        address_mode = "host"
        name = "psql-keeper"
        check {
          type = "tcp"
          port = "psql_port"
          interval = "60s"
          timeout = "5s"
        }
      }
    }
  }
}

