```
curl http://127.0.0.1:8500/v1/kv/traefik/acme/account/object?raw > traefik.gzip
gunzip -c traefik.gzip > traefik.json
cat traefik.json | jq '.DomainsCertificate.Certs[] | .Certificate.Domain, .Domains.Main'
# "alps.deuxfleurs.fr"
# "alps.deuxfleurs.fr"
# "cloud.deuxfleurs.fr"
# "cloud.deuxfleurs.fr"
# chaque NDD doit apparaitre 2x à la suite sinon fix comme suit
cat traefik.json | jq > traefik-new.json
vim traefik-new.json
# enlever les certifs corrompus, traefik les renouvellera automatiquement au démarrage
gzip -c traefik-new.json > traefik-new.gzip
curl --request PUT --data-binary @traefik-new.gzip http://127.0.0.1:8500/v1/kv/traefik/acme/account/object
```
