# ANSIBLE

## How to proceed

For each machine, **one by one** do:
  - Check that cluster is healthy
    - Check gluster
      - `sudo gluster peer status`
      - `sudo gluster volume status all` (check Online Col, only `Y` must appear)
    - Check that Nomad is healthy
      - `nomad server members`
      - `nomad node status`
    - Check that Consul is healthy
      - `consul members`
    - Check that Postgres is healthy
  - Run `ansible-playbook -i production.yml --limit <machine> -u <username> site.yml`
  - Run `nomad node drain -enable -force -self`
  - Reboot
  - Run `nomad node drain -self -disable`
  - Check that cluster is healthy

